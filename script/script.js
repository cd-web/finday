$(document).ready(function() {

  //
  // Плавная прокрутка при нажатие на якорь
  //

  $("a[href*='#']").click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target || $('[name=' + this.hash.slice(1) + ']');
      if ($target.length) {
        var targetOffset = $target.offset().top;
        $('html,body')
          .animate({ scrollTop: targetOffset }, 1000);
        return false;
      }
    }
  });

  //
  // Появление или скрытие поиска
  //

  $("body").on("click", ".search .ico", function() {
    $(".search .ico").removeClass("ico").addClass("ico-cross");
    $(".search .search-group").css("left", "0");
  });

  $("body").on("click", ".search .ico-cross", function() {
    $(".search .ico-cross").removeClass("ico-cross").addClass("ico");
    $(".search .search-group").css("left", "100%");
  });

  //
  // Появление или скрытие модуля "Финансистка года" (спойлер на Main Page)
  //

  $("body").on("click", ".spoiler.close .title", function() {
    $(".spoiler").removeClass("close").addClass("open");
  });

  $("body").on("click", ".spoiler.open .title", function() {
    $(".spoiler").removeClass("open").addClass("close");
  });

  //
  // Выбрали или отменили выбор кандидата (vote)
  //

  var count = 0;

  $("body").on("click", ".vote-btn.inactive", function() {
    $(this).removeClass('inactive').addClass("active");
    $(this).text("номинант выбран");
    $(".vote-count").text(++count);
    if (count) {$(".content-info").removeClass("hide");}
  });

  $("body").on("click", ".vote-btn.active", function() {
    $(this).removeClass("active").addClass("inactive");
    $(this).text("отдать голос");
    $(".vote-count").text(--count);
    if (!count) {$(".content-info").addClass("hide");}
  });

  //
  // Меняет текст при наведении (vote)
  //

  $("body").on("mouseenter", ".vote-btn.active", function() {
    $(this).text("отменить выбор?");
  });

  $("body").on("mouseleave", ".vote-btn.active", function() {
    $(this).text("номинант выбран");
  });

  //
  // Прилипающая боковая панель
  //

  $(".sticky").stick_in_parent({
    offset_top: 30,
    parent: ".main-aside"
  });

  //
  // Слайдер на главной странице
  //

  $('.slider-img').unslider({
    animation: 'fade',
    autoplay: true,
    arrows: false,
    speed: 1000,
    delay: 5000
  });

  //
  // Троеточие и дата в конце заголовка (catalog press)
  //

  $(".dot-data").dotdotdot({
    after: "span.data"
  });

  //
  // Сетка вывода фотографий в альбоме (photo gallery)
  //

  $('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 176,
    gutter: 20
  });

});

//
// Появление или скрытие Push Уведомления
//

function showPush() {
  $('.push').fadeIn("slow");
};

function hidePush() {
  $('.push').fadeOut("slow");
};

//
// Появление или скрытие модального окна
//

function showModal(selector) {
  // $("html").css("overflow","hidden");
  $('.blackout').fadeIn("fast");
  $('.blackout' + ' .window.' + selector).fadeIn("slow");
};

function hideModal() {
  // $("html").css("overflow","auto");
  $('.blackout').fadeOut("slow");
  $('.blackout' + ' .window').fadeOut("slow");
};

//
// Смена модального окна (первое скрыть, второе показать)
//

function changeModal(selector1, selector2) {
  $(selector1).fadeOut("fast");
  $(selector2).fadeIn("fast");
};

//
// Input File
//

$(function() {
  $(document).on('change', ':file', function() {
    var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  $(document).ready(function() {
    $(':file').on('fileselect', function(event, numFiles, label) {
      console.log(numFiles);
      console.log(label);
      var input = $(this).parents('.file-input').find(':text'),
        log = numFiles > 1 ? numFiles + ' files selected' : label;
      if (input.length) { input.val(log); }
      else              { if (log) alert(log); }
    });
  });
});
